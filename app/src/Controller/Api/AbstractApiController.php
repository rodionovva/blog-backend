<?php

namespace App\Controller\Api;

use App\Entity\Device;
use App\Entity\User;
use App\Service\ValidatorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class AbstractApiController extends AbstractController
{
    public function __construct(
        protected SerializerInterface    $serializer,
        protected EntityManagerInterface $entityManager,
        protected ValidatorService       $validatorService,
        protected DenormalizerInterface  $denormalizer,
        protected RequestStack           $requestStack,
    )
    {
    }
}