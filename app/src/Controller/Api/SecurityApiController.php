<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Helper\ResponseAttributes\NoValidDataResponse;
use App\Security\SecurityService;
use OpenApi\Attributes as OA;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[OA\Tag('Безопасность')]
#[Route(path: '/security')]
class SecurityApiController extends AbstractApiController
{
    #[OA\Post(
        description: 'Метод нужен для авторизации пользователей. В теле запроса передаем логин и пароль, в ответ получаем
        логин и токен, либо 401 ошибку, если данные неверные',
        summary: 'Вход по логину и паролю',
        requestBody: new OA\RequestBody(
            required: true,
            content: new OA\JsonContent(
                properties: [
                    new OA\Property(property: 'username', description: 'Идентификатор пользователя', type: 'string'),
                    new OA\Property(property: 'password', description: 'Пароль пользователя', type: 'string'),
                ],
            ),
        ),
        responses: [
            new OA\Response(
                response: Response::HTTP_OK,
                description: 'Вход выполнен',
                content: new OA\JsonContent(
                    properties: [
                        new OA\Property('user', description: 'Идентификатор пользователя', type: 'string'),
                        new OA\Property('token', description: 'Токен пользователя', type: 'string'),
                    ],
                ),
            ),
            new OA\Response(
                response: Response::HTTP_UNAUTHORIZED,
                description: 'Неверные данные',
                content: new OA\JsonContent(
                    properties: [
                        new OA\Property('error', description: 'Ошибка при входе', type: 'string', example: 'Invalid credentials.'),
                    ],
                ),
            ),
        ],
    )]
    #[Route(path: '', name: 'loginUser', methods: ['POST'])]
    #[NoValidDataResponse]
    //TODO: переопределить ошибку при неверных креденшенах
    public function login(SecurityService $securityService, #[CurrentUser] ?User $user = null): JsonResponse
    {
        return $this->json($securityService->login($user));
    }

    #[OA\Delete(
        description: 'Метод нужен для выхода. Отправляем запрос без параметров, в результате выполнения токену, с которым
        пользователь вызвал метод, получит неактивный статус.',
        summary: 'Выход',
        responses: [
            new OA\Response(
                response: Response::HTTP_NO_CONTENT,
                description: 'Успешно! Токен деактивирован',
            )
        ]
    )]
    #[Route(path: '', name: 'logoutUser', methods: ['DELETE'])]
    public function logout(Request $request, SecurityService $securityService): JsonResponse
    {
        $securityService->logout($request->headers->get('apiKey'));
        return $this->json([], status: Response::HTTP_NO_CONTENT);
    }
}