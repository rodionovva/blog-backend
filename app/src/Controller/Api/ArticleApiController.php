<?php

namespace App\Controller\Api;

use App\Exception\JsonInvalidException;
use App\Helper\DTO\Create\ArticleCreateDTO;
use App\Helper\Filter\PaginationFilter;
use App\Helper\QueryParametersAttributes\PageCountParameter;
use App\Helper\QueryParametersAttributes\PageNumberParameter;
use App\Helper\ResponseAttributes\NotFoundResponse;
use App\Helper\ResponseAttributes\NoValidDataResponse;
use App\Helper\ResponseAttributes\UnauthorizedResponse;
use App\Service\ArticleService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Attributes as OA;
use Symfony\Component\Routing\Annotation\Route;
use App\Helper\DTO\Response\ArticleResponseDTO;

#[OA\Tag('Статья')]
#[Route(path: '/article')]
#[UnauthorizedResponse]
final class ArticleApiController extends AbstractApiController
{
    #[OA\Post(
        description: 'Метод нужен для создания статьи. В теле запроса передаем необходимую информацию о статье, в результате
        выполнения запроса создается статья, помещается в БД и возвращается в ответе на запрос с кодом ответа 201.',
        summary: 'Создание статьи',
        requestBody: new OA\RequestBody(
            required: true,
            content: new OA\JsonContent(
                ref: new Model(type: ArticleCreateDTO::class, groups: ['create']),
            ),
        ),
        responses: [
            new OA\Response(
                response: Response::HTTP_CREATED,
                description: 'Успешно! Статья создана.',
                content: new OA\JsonContent(
                    ref: new Model(type: ArticleResponseDTO::class, groups: ['getArticle', 'getFile']),
                ),
            ),
        ],
    )]
    #[NotFoundResponse]
    #[NoValidDataResponse]
    #[Route(path: '', name: 'createArticle', methods: ['POST'])]
    public function create(Request $request, ArticleService $articleService): JsonResponse
    {
        try {
            $articleCreateDTO = $this->serializer->deserialize($request->getContent(), ArticleCreateDTO::class, 'json');
        } catch (\Exception $e) {
            throw new JsonInvalidException($e->getMessage());
        }
        $this->validatorService->validate($articleCreateDTO, ['create']);
        return $this->json($articleService->create($articleCreateDTO, $this->getUser()), status: Response::HTTP_CREATED, context: ['groups' => ['getArticle', 'getFile']]);
    }

    #[OA\Get(
        description: 'Метод нужен для получения статьи по ее id. В пути запроса указываем id статьи, в ответ получаем
        статью, если она найдена. Если указать несуществующий id, то вернется 404 ошибка',
        summary: 'Получение статьи по id',
        parameters: [
            new OA\Parameter(
                name: 'articleId',
                description: 'id статьи',
                in: 'path',
                required: true,
                schema: new OA\Schema(type: 'integer'),
            ),
        ],
        responses: [
            new OA\Response(
                response: Response::HTTP_OK,
                description: 'Успешно! Статья найдена и получена.',
                content: new OA\JsonContent(
                    ref: new Model(type: ArticleResponseDTO::class, groups: ['getArticle', 'getFile']),
                ),
            ),
        ],
    )]
    #[NotFoundResponse]
    #[Route(path: '/{articleId<\d+>}', name: 'getArticleById', methods: ['GET'])]
    public function getById(string $articleId, ArticleService $articleService): JsonResponse
    {
        $this->validatorService->validateId($articleId);
        return $this->json($articleService->getById($articleId), context: ['groups' => ['getArticle', 'getFile']]);
    }

    #[OA\Get(
        description: 'Метод нужен для получения статей текущего пользователя. В квери параметрах запроса указываем параметры
        пагинации. В ответ получаем массив статей текущего пользователя',
        summary: 'Получение статей текущего пользователя',
        responses: [
            new OA\Response(
                response: Response::HTTP_OK,
                description: 'Успешно! Статьи пользователя получены',
                content: new OA\JsonContent(
                    type: 'array',
                    items: new OA\Items(ref: new Model(type: ArticleResponseDTO::class, groups: ['getArticle', 'getFile'])),
                ),
            ),
        ],
    )]
    #[NoValidDataResponse]
    #[PageCountParameter]
    #[PageNumberParameter]
    #[Route(path: '/user', name: 'getArticlesCurrentUser', methods: ['GET'])]
    public function getMyArticle(Request $request, ArticleService $articleService): JsonResponse
    {
        $paginationFilter = $this->denormalizer->denormalize($request->query->all(), PaginationFilter::class);
        $this->validatorService->validate(query: $paginationFilter, groupsQuery: ['pagination']);
        return $this->json($articleService->getArticlesByUser($paginationFilter, $this->getUser()), context: ['groups' => ['getArticle', 'getFile']]);
    }

    #[OA\Get(
        description: 'Метод нужен для получения всех статей. В квери запроса указываем параметры пагинации, в ответ получаем
        список статей от новых к старым.',
        summary: 'Получение всех статей',
        responses: [
            new OA\Response(
                response: Response::HTTP_OK,
                description: 'Успешно! Список статей получен.',
                content: new OA\JsonContent(
                    type: 'array',
                    items: new OA\Items(ref: new Model(type: ArticleResponseDTO::class, groups: ['getArticle', 'getFile'])),
                ),
            ),
        ],
    )]
    #[NoValidDataResponse]
    #[PageCountParameter]
    #[PageNumberParameter]
    #[Route(path: '', name: 'getAllArticles', methods: ['GET'])]
    public function getAllArticles(Request $request, ArticleService $articleService): JsonResponse
    {
        $paginationFilter = $this->denormalizer->denormalize($request->query->all(), PaginationFilter::class);
        $this->validatorService->validate(query: $paginationFilter, groupsQuery: ['pagination']);
        return $this->json($articleService->getAllArticles($paginationFilter), context: ['groups' => ['getArticle', 'getFile']]);
    }

    #[OA\Get(
        description: 'Метод нужен для получения статей конкретного пользователя. В пути запроса указываем id пользователя,
        в квери параметры пагинации. Если пользователь найден, то вернется список его статей от новых к старым',
        summary: 'Получение статей пользователя',
        responses: [
            new OA\Response(
                response: Response::HTTP_OK,
                description: 'Успешно! Список статей получен.',
                content: new OA\JsonContent(
                    type: 'array',
                    items: new OA\Items(ref: new Model(type: ArticleResponseDTO::class, groups: ['getArticle', 'getFile'])),
                ),
            ),
        ],
    )]
    #[NoValidDataResponse]
    #[PageCountParameter]
    #[PageNumberParameter]
    #[Route(path: '/user/{userId<\d+>}', name: 'getArticleByUser', methods: ['GET'])]
    public function getArticlesByUser(string $userId, Request $request, ArticleService $articleService): JsonResponse
    {
        $this->validatorService->validateId($userId);
        $paginationFilter = $this->denormalizer->denormalize($request->query->all(), PaginationFilter::class);
        $this->validatorService->validate(query: $paginationFilter, groupsQuery: ['pagination']);
        return $this->json($articleService->getArticlesByUserId($paginationFilter, $userId), context: ['groups' => ['getArticle', 'getFile']]);
    }

    #[OA\Delete(
        description: 'Метод нужен для архивирования статьи. В теле запроса указываем id статьи. В результате выполнения
        запроса указанная статья помечается как удаленная. При успешном выполнении вернется 204 код',
        summary: 'Архивирование статьи',
        parameters: [
            new OA\Parameter(
                name: 'articleId',
                description: 'ID статьи',
                in: 'path',
                required: true,
                schema: new OA\Schema(type: 'integer'),
            ),
        ],
        responses: [
            new OA\Response(
                response: Response::HTTP_NO_CONTENT,
                description: 'Успешно! Статья помечена как удаленная',
            ),
        ],
    )]
    #[NotFoundResponse]
    #[Route(path: '/{articleId<\d+>}', name: 'deleteArticle', methods: ['DELETE'])]
    public function deleteArticle(string $articleId, ArticleService $articleService): JsonResponse
    {
        $this->validatorService->validateId($articleId);
        $articleService->removeArticle($articleId, $this->getUser());
        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}