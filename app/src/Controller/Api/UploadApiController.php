<?php

namespace App\Controller\Api;

use App\Helper\ResponseAttributes\NotFoundResponse;
use App\Helper\ResponseAttributes\NoValidDataResponse;
use App\Helper\ResponseAttributes\UnauthorizedResponse;
use App\Service\UploadService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Attributes as OA;
use Symfony\Component\Routing\Annotation\Route;

#[OA\Tag('Загрузка файла')]
#[Route(path: '/uploads')]
#[UnauthorizedResponse]
final class UploadApiController extends AbstractApiController
{
    #[OA\Post(
        description: 'Метод нужен для загрузки файла. Отправляем файл в теле запроса, в ответ получаем id его записи в бд
        и путь к файлу либо ссылку на него',
        summary: 'Загрузка файла',
        requestBody: new OA\RequestBody(
            required: true,
            content: new OA\MediaType(
                mediaType: 'multipart/form-data',
                schema: new OA\Schema(
                    properties: [
                        new OA\Property(property: 'file', type: 'file'),
                    ],
                ),
            ),
        ),
        responses: [
            new OA\Response(
                response: Response::HTTP_CREATED,
                description: 'Успешно! Файл загружен.',
                content: new OA\JsonContent(
                    ref: new Model(type: 'App\Entity\File', groups: ['getFile']),
                ),
            ),
        ]
    )]
    #[NoValidDataResponse]
    #[Route(path: '', name: 'uploadFile', methods: ['POST'])]
    public function uploadFile(Request $request, UploadService $uploadService): JsonResponse
    {
        $file = $request->files->get('file');
        $this->validatorService->validateFile($uploadService->getRequirements(), $file);
        return $this->json($uploadService->uploadFile($file), Response::HTTP_CREATED);
    }

    #[OA\Delete(
        description: 'Метод нужен для удаления файла. В пути запроса указываем id записи файла в бд, в результате выполнения
        файл и его запись в бд удалятся',
        summary: 'Удаление файла',
        responses: [
            new OA\Response(
                response: Response::HTTP_NO_CONTENT,
                description: 'Успешно! Файл удален.',
            ),
        ],
    )]
    #[NotFoundResponse]
    #[Route(path: '', name: 'deleteFile', methods: ['DELETE'])]
    public function delete(string $fileId, UploadService $uploadService): JsonResponse
    {
        $this->validatorService->validateId($fileId);
        $uploadService->deleteFile($fileId);
        return $this->json([], status: Response::HTTP_NO_CONTENT);
    }
}