<?php

namespace App\Controller\Api;

use App\Exception\JsonInvalidException;
use App\Helper\DTO\Create\UserCreateDTO;
use App\Helper\ResponseAttributes\NoValidDataResponse;
use App\Service\UserService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Attributes as OA;
use Symfony\Component\Routing\Annotation\Route;

#[OA\Tag('Пользователь')]
#[Route(path: '/user')]
final class UserApiController extends AbstractApiController
{
    #[OA\Post(
        description: 'Метод нужен для регистрации пользователя. В теле запроса указываем необходимую о пользователе
        информацию, в случае успеха получаем ответ с 201 кодом и данными о созданном пользователе.',
        summary: 'Создание пользователя',
        requestBody: new OA\RequestBody(
            required: true,
            content: new OA\JsonContent(
                ref: new Model(type: 'App\Helper\DTO\Create\UserCreateDTO', groups: ['create']),
            ),
        ),
        responses: [
            new OA\Response(
                response: Response::HTTP_CREATED,
                description: 'Успешно! Создание пользователя.',
                content: new OA\JsonContent(
                    ref: new Model(type: 'App\Helper\DTO\Response\UserResponseDTO', groups: ['getUser']),
                ),
            ),
        ],
    )]
    #[NoValidDataResponse]
    #[Route(path: '', name: 'registrationCreateUser', methods: ['POST'])]
    public function create(Request $request, UserService $userService): JsonResponse
    {
        try {
            $userCreateDTO = $this->serializer->deserialize($request->getContent(), UserCreateDTO::class, 'json');
        } catch (\Exception $e) {
            throw new JsonInvalidException($e->getMessage());
        }
        $this->validatorService->validate(body: $userCreateDTO, groupsBody: ['create']);
        return $this->json($userService->create($userCreateDTO), status: Response::HTTP_CREATED, context: ['groups' => ['getUser']]);
    }
}