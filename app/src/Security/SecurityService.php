<?php

namespace App\Security;

use App\Entity\Device;
use App\Entity\User;
use App\Exception\AuthException;
use App\Helper\Status\DeviceStatus;
use Doctrine\ORM\EntityManagerInterface;

final readonly class SecurityService
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
    )
    {
    }

    /**
     * @param User|null $user
     * @return array
     */
    public function login(?User $user): array
    {
        if (!$user) {
            throw new AuthException('Неверный логин/пароль.');
        }
        $device = (new Device())
            ->setOwner($user);
        $this->entityManager->persist($device);
        $this->entityManager->flush();
        return [
            'user' => $user->getUserIdentifier(),
            'token' => $device->getToken(),
        ];
    }

    /**
     * @param string $token
     * @return void
     */
    public function logout(string $token): void
    {
        $device = $this->entityManager->getRepository(Device::class)->findOneBy([
            'token' => $token,
        ]);
        if ($device) {
            $device
                ->setStatus(DeviceStatus::NOT_ACTIVE->value);
        }
        $this->entityManager->flush();
    }
}