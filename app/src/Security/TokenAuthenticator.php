<?php

namespace App\Security;

use App\Entity\Device;
use App\Exception\AuthException;
use App\Helper\Status\DeviceStatus;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

final class TokenAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
    )
    {
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has('apiKey');
    }

    public function authenticate(Request $request): Passport
    {
        $token = $request->headers->get('apiKey');
        $device = $this->entityManager->getRepository(Device::class)->findOneBy([
            'token' => $token,
            'status' => DeviceStatus::ACTIVE->value,
        ]);
        if (!$device) {
            throw new AuthException('Токен авторизации не определен или не активен.');
        }
        return new SelfValidatingPassport(new UserBadge($device->getOwner()->getLogin()));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        throw new AuthException($exception->getMessage());
    }
}
