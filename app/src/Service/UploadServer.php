<?php

namespace App\Service;

use App\Entity\File;
use App\Exception\UploadFileException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final readonly class UploadServer implements UploadInterface
{
    public function __construct(
        protected Filesystem $filesystem,
        protected string     $directory,
    )
    {
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return string
     */
    public function new(UploadedFile $uploadedFile): string
    {
        $filename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_BASENAME);
        $newName = uniqid() . '_' . str_replace(' ', '_', $filename);
        try {
            if (!$this->filesystem->exists($this->directory)) {
                $this->filesystem->mkdir($this->directory);
            }
            $path = $this->directory . $newName;
            copy($uploadedFile, $path);
        } catch (\Exception $e) {
            throw new UploadFileException($e->getMessage());
        }
        return $path;
    }

    public function delete(File $file): void
    {
        unlink($file->getPath());
    }

    public function getRequirements(): array
    {
        return [
            'maxSize' => '20M',
            'mimeTypes' => [
                'image/jpeg',
                'image/jpg',
                'image/png',
            ]
        ];
    }
}