<?php

namespace App\Service;

use App\Exception\ApiException;
use App\Exception\UploadFileException;
use App\Exception\EntityNotFoundException;
use App\Helper\HelperTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final readonly class ValidatorService
{
    use HelperTrait;

    public function __construct(
        protected ValidatorInterface $validator,
    )
    {
    }

    public function validate($body = [], $groupsBody = [], $query = [], $groupsQuery = []): void
    {
        $bodyErrors = $this->validator->validate($body, groups: $groupsBody);
        $validationErrors = [
            'body' => [],
            'query' => [],
        ];
        foreach ($bodyErrors as $error) {
            $validationErrors['body'][] = [
                'name' => $error->getPropertyPath(),
                'message' => $error->getMessage(),
            ];
        }
        $queryErrors = $this->validator->validate($query, groups: $groupsQuery);
        foreach ($queryErrors as $error) {
            $validationErrors['query'][] = [
                'name' => $error->getPropertyPath(),
                'message' => $error->getMessage(),
            ];
        }
        if (count($validationErrors['body']) > 0 || count($validationErrors['query'])) {
            throw new ApiException(
                'Ошибки валидациии',
                'Validation errors',
                Response::HTTP_CONFLICT,
                validationArray: $validationErrors,
            );
        }
    }

    public function validateFile(array $requirements, ?UploadedFile $uploadedFile): void
    {
        if ($uploadedFile === null) {
            throw new UploadFileException('Пустой параметр file.');
        }
        $violations = $this->validator->validate(
            $uploadedFile,
            new File($requirements)
        );

        if (count($violations) > 0) {
            throw new UploadFileException('Не валидный формат или размер файла превышен.');
        }
    }

    public function validateId(int|string $id): void
    {
        if (!is_int((int)$id) or $id > self::MAX_SIZE_INTEGER or $id < self::MIN_SIZE_INTEGER) {
            throw new EntityNotFoundException();
        }
    }
}