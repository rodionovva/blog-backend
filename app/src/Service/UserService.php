<?php

namespace App\Service;

use App\Entity\User;
use App\Helper\DTO\Create\UserCreateDTO;
use App\Helper\DTO\Response\UserResponseDTO;
use App\Helper\Mapper\MapperInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final readonly class UserService
{
    public function __construct(
        protected MapperInterface             $mapper,
        protected UserPasswordHasherInterface $userPasswordHasher,
        protected EntityManagerInterface      $entityManager,
    )
    {
    }

    public function create(UserCreateDTO $userCreateDTO): UserResponseDTO
    {
        /** @var User $user */
        $user = $this->mapper->getEntityFromDTO($userCreateDTO);
        $passwordHash = $this->userPasswordHasher->hashPassword(
            $user,
            $userCreateDTO->password,
        );
        $user
            ->setPassword($passwordHash);
        $this->entityManager->flush();
        return $this->mapper->getDTOFromEntity($user);
    }
}