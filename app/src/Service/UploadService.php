<?php

namespace App\Service;

use App\Entity\File;
use App\Exception\FileNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final readonly class UploadService
{
    public function __construct(
        protected UploadInterface        $upload,
        protected EntityManagerInterface $entityManager,
    )
    {
    }

    /**
     * @param string $fileId
     * @return File
     */
    private function findById(string $fileId): File
    {
        $file = $this->entityManager->getRepository(File::class)->find($fileId);
        if (!$file) {
            throw new FileNotFoundException();
        }
        return $file;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return array
     */
    public function uploadFile(UploadedFile $uploadedFile): array
    {
        $path = $this->upload->new($uploadedFile);
        $file = (new File())
            ->setPath($path);
        $this->entityManager->persist($file);
        $this->entityManager->flush();
        return [
            'id' => $file->getId(),
            'path' => $file->getPath(),
        ];
    }

    /**
     * @param string $fileId
     * @return void
     */
    public function deleteFile(string $fileId): void
    {
        $file = $this->findById($fileId);
        $this->upload->delete($file);
        $this->entityManager->remove($file);
        $this->entityManager->flush();
    }

    /**
     * @return array
     */
    public function getRequirements(): array
    {
        return $this->upload->getRequirements();
    }
}