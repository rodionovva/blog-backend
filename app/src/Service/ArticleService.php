<?php

namespace App\Service;

use App\Entity\Article;
use App\Entity\User;
use App\Exception\ArticleDoesNotBelongCurrentUserException;
use App\Exception\ArticleNotFoundException;
use App\Exception\UserNotFoundException;
use App\Helper\DTO\Create\ArticleCreateDTO;
use App\Helper\DTO\Response\ArticleResponseDTO;
use App\Helper\Filter\PaginationFilter;
use App\Helper\Mapper\MapperInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final readonly class ArticleService
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected MapperInterface        $mapper,
    )
    {
    }

    private function findById(string $articleId): Article
    {
        $article = $this->entityManager->getRepository(Article::class)->find($articleId);
        if (!$article) {
            throw new ArticleNotFoundException();
        }
        return $article;
    }

    /**
     * @param ArticleCreateDTO $articleCreateDTO
     * @param User $currentUser
     * @return ArticleResponseDTO
     */
    public function create(ArticleCreateDTO $articleCreateDTO, UserInterface $currentUser): ArticleResponseDTO
    {
        /** @var Article $article */
        $article = $this->mapper->getEntityFromDTO($articleCreateDTO);
        $article
            ->setCreatedUser($currentUser);
        $this->entityManager->flush();
        return $this->mapper->getDTOFromEntity($article);
    }

    /**
     * @param string $articleId
     * @return ArticleResponseDTO
     */
    public function getById(string $articleId): ArticleResponseDTO
    {
        $article = $this->findById($articleId);
        return $this->mapper->getDTOFromEntity($article);
    }

    /**
     * @param PaginationFilter $paginationFilter
     * @param UserInterface $user
     * @return array
     */
    public function getArticlesByUser(PaginationFilter $paginationFilter, UserInterface $user): array
    {
        /** @var Article[] $articles */
        $articles = $this->entityManager->getRepository(Article::class)->findBy(
            [
                'createdUser' => $user,
            ],
            orderBy: [
                'id' => 'DESC',
            ],
            limit: $paginationFilter->getLimit(),
            offset: $paginationFilter->getOffset(),
        );
        $result = [];
        foreach ($articles as $article) {
            $result[] = $this->mapper->getDTOFromEntity($article);
        }
        return $result;
    }

    /**
     * @param PaginationFilter $paginationFilter
     * @param string $userId
     * @return array
     */
    public function getArticlesByUserId(PaginationFilter $paginationFilter, string $userId): array
    {
        $user = $this->entityManager->getRepository(User::class)->find($userId);
        if (!$user) {
            throw new UserNotFoundException();
        }
        return $this->getArticlesByUser($paginationFilter, $user);
    }

    /**
     * @param PaginationFilter $paginationFilter
     * @return array
     */
    public function getAllArticles(PaginationFilter $paginationFilter): array
    {
        $this->entityManager->getFilters()->disable('softdeleteable');
        $articles = $this->entityManager->getRepository(Article::class)->findBy(
            [],
            orderBy: ['id' => 'DESC'],
            limit: $paginationFilter->getLimit(),
            offset: $paginationFilter->getOffset(),
        );
        $result = [];
        foreach ($articles as $article) {
            $result[] = $this->mapper->getDTOFromEntity($article);
        }
        return $result;
    }

    /**
     * @param string $articleId
     * @param UserInterface $user
     * @return void
     */
    public function removeArticle(string $articleId, UserInterface $user): void
    {
        $article = $this->findById($articleId);
        if (!$user->getArticles()->contains($article)) {
            throw new ArticleDoesNotBelongCurrentUserException();
        }
        $this->entityManager->remove($article);
        $this->entityManager->flush();
    }
}