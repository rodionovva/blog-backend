<?php

namespace App\Service;

use App\Entity\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface UploadInterface
{
    public function new(UploadedFile $uploadedFile): string;

    public function delete(File $file): void;

    public function getRequirements(): array;
}