<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

final class ArticleNotFoundException extends ApiException
{
    private const MESSAGE = 'Статья не найдена';

    private const DETAIL = 'Article not found';

    public function __construct()
    {
        parent::__construct(
            self::MESSAGE,
            self::DETAIL,
            Response::HTTP_NOT_FOUND,
        );
    }
}