<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

final class FileNotFoundException extends ApiException
{
    private const MESSAGE = 'Файл не найден';

    private const DETAIL = 'File not found';

    public function __construct()
    {
        parent::__construct(
            self::MESSAGE,
            self::DETAIL,
            Response::HTTP_NOT_FOUND,
        );
    }
}