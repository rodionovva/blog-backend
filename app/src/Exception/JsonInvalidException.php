<?php

namespace App\Exception;

final class JsonInvalidException extends ApiException
{
    private const MESSAGE = 'Невалидный json';

    public function __construct(string $detail)
    {
        parent::__construct(
            self::MESSAGE,
            $detail,
        );
    }
}