<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

final class EntityNotFoundException extends ApiException
{
    const MESSAGE = 'Сущность не найдена';

    const DETAIL = 'Entity not found';

    public function __construct()
    {
        parent::__construct(
            self::MESSAGE,
            self::DETAIL,
            Response::HTTP_NOT_FOUND,
        );
    }
}