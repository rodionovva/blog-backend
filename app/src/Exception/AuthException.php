<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

final class AuthException extends ApiException
{
    private const MESSAGE = 'Ошибка авторизации.';

    public function __construct(
        string $detail,
    )
    {
        parent::__construct(
            self::MESSAGE,
            $detail,
            Response::HTTP_UNAUTHORIZED,
        );
    }
}