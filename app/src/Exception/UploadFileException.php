<?php

namespace App\Exception;

final class UploadFileException extends ApiException
{
    private const MESSAGE = 'Ошибка во время загрузки файла';

    public function __construct(string $detail)
    {
        parent::__construct(
            self::MESSAGE,
            $detail,
        );
    }
}