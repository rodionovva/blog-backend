<?php

namespace App\Exception;

final class ArticleDoesNotBelongCurrentUserException extends ApiException
{
    private const MESSAGE = 'Статья не принадлежит текущему пользователю';

    private const DETAIL = 'Article does not belong to current user';

    public function __construct()
    {
        parent::__construct(
            self::MESSAGE,
            self::DETAIL,
        );
    }
}