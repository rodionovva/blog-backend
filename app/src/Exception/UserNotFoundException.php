<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

final class UserNotFoundException extends ApiException
{
    private const MESSAGE = 'Пользователь не найден';

    private const DETAIL = 'User not found';

    public function __construct()
    {
        parent::__construct(
            self::MESSAGE,
            self::DETAIL,
            Response::HTTP_NOT_FOUND,
        );
    }
}