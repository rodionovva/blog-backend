<?php

namespace App\EventListener;

use App\Exception\ApiException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

final readonly class ExceptionListener
{
    public function __invoke(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if ($exception instanceof ApiException) {
            $response = new JsonResponse($exception->response(), $exception->getStatusCode());
        } elseif ($exception instanceof HttpException) {
            throw new ApiException(
                $exception->getMessage(),
                status: $exception->getStatusCode(),
            );
        } else {
            throw new ApiException(
                $exception->getMessage(),
                status: Response::HTTP_BAD_REQUEST,
            );
        }
        $event->setResponse($response);
    }
}