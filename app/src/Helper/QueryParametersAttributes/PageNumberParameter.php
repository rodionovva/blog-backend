<?php

namespace App\Helper\QueryParametersAttributes;

use OpenApi\Attributes as OA;

#[\Attribute] final class PageNumberParameter extends OA\Parameter
{
    public function __construct()
    {
        parent::__construct(
            name: 'pageNumber',
            description: 'Номер страницы (от 1)',
            in: 'query',
            required: false,
            schema: new OA\Schema(type: 'integer'),
        );
    }
}