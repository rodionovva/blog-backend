<?php

namespace App\Helper\QueryParametersAttributes;

use OpenApi\Attributes as OA;

#[\Attribute] final class PageCountParameter extends OA\Parameter
{
    public function __construct()
    {
        parent::__construct(
            name: 'pageCount',
            description: 'Количество элементов на странице (от 0 до 100)',
            in: 'query',
            required: false,
            schema: new OA\Schema(type: 'integer'),
        );
    }
}