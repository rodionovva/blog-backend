<?php

namespace App\Helper;

trait HelperTrait
{
    public const MAX_SIZE_INTEGER = 2147483647;
    public const MIN_SIZE_INTEGER = -2147483647;
    public const DEFAULT_LIMIT = 10;
    public const DEFAULT_OFFSET = 0;
}