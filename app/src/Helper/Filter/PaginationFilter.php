<?php

namespace App\Helper\Filter;

use App\Helper\HelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

final class PaginationFilter
{
    use HelperTrait;

    #[Assert\PositiveOrZero(message: 'Значение должно быть неотрицательным', groups: ['pagination'])]
    #[Assert\Range(notInRangeMessage: 'Допустимые значения от 0 до 100', min: 0, max: 100, groups: ['pagination'])]
    public $pageCount = null;

    #[Assert\PositiveOrZero(message: 'Значение должно быть неотрицательным', groups: ['pagination'])]
    #[Assert\Range(minMessage: 'Допустимые значения от 1', min: 1, groups: ['pagination'])]
    public $pageNumber = null;

    public function getLimit(): int
    {
        return $this->pageCount ?? self::DEFAULT_LIMIT;
    }

    public function getOffset(): int
    {
        if ($this->pageCount && $this->pageNumber) {
            $res = $this->pageCount * ($this->pageNumber - 1);
        } else {
            $res = self::DEFAULT_OFFSET;
        }
        return $res;
    }
}