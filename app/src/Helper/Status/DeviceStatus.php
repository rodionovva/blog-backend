<?php

namespace App\Helper\Status;

enum DeviceStatus: int implements StatusInterface
{
    case ACTIVE = 1;

    case NOT_ACTIVE = 10;

    public function getStatus(): string
    {
        return match ($this) {
            self::ACTIVE => 'Активный',
            self::NOT_ACTIVE => 'Не активный',
        };
    }
}