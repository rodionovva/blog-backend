<?php

namespace App\Helper\Status;

interface StatusInterface
{
    public function getStatus(): string;
}