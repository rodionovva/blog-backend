<?php

namespace App\Helper\ResponseAttributes;

use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

#[\Attribute] final class NoValidDataResponse extends OA\Response
{
    public function __construct()
    {
        parent::__construct(
            response: Response::HTTP_CONFLICT,
            description: 'Ошибки валидации',
            content: new OA\JsonContent(ref: '#/components/schemas/ApiException'),
        );
    }
}