<?php

namespace App\Helper\ResponseAttributes;
use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

#[\Attribute] final class UnauthorizedResponse extends OA\Response
{
    public function __construct()
    {
        parent::__construct(
            response: Response::HTTP_UNAUTHORIZED,
            description: 'Ошибка авторизации',
            content: new OA\JsonContent(ref: '#/components/schemas/ApiException'),
        );
    }
}