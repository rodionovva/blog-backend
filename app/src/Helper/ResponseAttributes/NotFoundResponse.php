<?php

namespace App\Helper\ResponseAttributes;
use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

#[\Attribute] final class NotFoundResponse extends OA\Response
{
    public function __construct()
    {
        parent::__construct(
            response: Response::HTTP_NOT_FOUND,
            description: 'Сущность не найдена',
            content: new OA\JsonContent(ref: '#/components/schemas/ApiException')
        );
    }
}