<?php

namespace App\Helper\Mapper;

use App\Entity\User;
use App\Helper\DTO\Response\UserResponseDTO;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

final readonly class UserMapper implements MapperInterface
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
    )
    {
    }

    /**
     * @param $DTO
     * @param $entity
     * @return User
     * @throws Exception
     */
    public function getEntityFromDTO($DTO, $entity = null): User
    {
        if (!$entity) {
            $user = new User();
            $this->entityManager->persist($user);
        } else {
            /** @var User $user */
            $user = $entity;
        }
        $user
            ->setName($DTO->name)
            ->setSurname($DTO->surname)
            ->setDateBorn(new \DateTime($DTO->dateBorn))
            ->setLogin($DTO->login);
        return $user;
    }

    /**
     * @param $entity
     * @return UserResponseDTO
     */
    public function getDTOFromEntity($entity): UserResponseDTO
    {
        return new UserResponseDTO($entity);
    }
}