<?php

namespace App\Helper\Mapper;

use App\Entity\Article;
use App\Entity\File;
use App\Exception\FileNotFoundException;
use App\Helper\DTO\Response\ArticleResponseDTO;
use Doctrine\ORM\EntityManagerInterface;

final readonly class ArticleMapper implements MapperInterface
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
    )
    {
    }

    /**
     * @param $DTO
     * @param $entity
     * @return Article
     */
    public function getEntityFromDTO($DTO, $entity = null): Article
    {
        if (!$entity) {
            $article = new Article();
            $this->entityManager->persist($article);
        } else {
            /** @var Article $article */
            $article = $entity;
        }
        $article
            ->setTitle($DTO->title)
            ->setContent($DTO->content);
        if (count($DTO->images) > 0) {
            $images = $this->entityManager->getRepository(File::class)->findById($DTO->images);
            foreach ($DTO->images as $imageId) {
                if (array_key_exists($imageId, $images)) {
                    $article
                        ->addImage($images[$imageId]);
                } else {
                    throw new FileNotFoundException();
                }
            }
        }
        return $article;
    }

    /**
     * @param $entity
     * @return ArticleResponseDTO
     */
    public function getDTOFromEntity($entity): ArticleResponseDTO
    {
        return new ArticleResponseDTO($entity);
    }
}