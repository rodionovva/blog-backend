<?php

namespace App\Helper\Mapper;

interface MapperInterface
{
    public function getEntityFromDTO($DTO, $entity = null);

    public function getDTOFromEntity($entity);
}