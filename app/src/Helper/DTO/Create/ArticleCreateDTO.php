<?php

namespace App\Helper\DTO\Create;

use OpenApi\Attributes\Items;
use OpenApi\Attributes\Property;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

final class ArticleCreateDTO
{
    #[Assert\Type(type: 'string', message: 'Неверный тип данных', groups: ['create'])]
    #[Assert\NotBlank(message: 'Значение не может быть пустым', groups: ['create'])]
    #[Property(type: 'string')]
    #[Groups(['create'])]
    public $title;

    #[Assert\Type(type: 'string', message: 'Неверный тип данных', groups: ['create'])]
    #[Assert\NotBlank(message: 'Значение не может быть пустым', groups: ['create'])]
    #[Property(type: 'string')]
    #[Groups(['create'])]
    public $content;

    #[Groups(['create'])]
    #[Property(type: 'array', items: new Items(type: 'integer'))]
    #[Assert\All(new Assert\Type(type: 'integer', message: 'Неверный тип данных', groups: ['create']))]
    public $images = [];
}