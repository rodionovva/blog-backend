<?php

namespace App\Helper\DTO\Create;

use OpenApi\Attributes\Property;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

final class UserCreateDTO
{
    #[Assert\Type(type: 'string', message: 'Неверный тип данных', groups: ['create'])]
    #[Assert\NotBlank(message: 'Значение не может быть пустым', groups: ['create'])]
    #[Property(type: 'string')]
    #[Groups(['create'])]
    public $name;

    #[Assert\Type(type: 'string', message: 'Неверный тип данных', groups: ['create'])]
    #[Assert\NotBlank(message: 'Значение не может быть пустым', groups: ['create'])]
    #[Property(type: 'string')]
    #[Groups(['create'])]
    public $surname;

    #[Assert\Type(type: 'string', message: 'Неверный тип данных', groups: ['create'])]
    #[Assert\NotBlank(message: 'Значение не может быть пустым', groups: ['create'])]
    #[Property(type: 'string', example: 'ГГГГ-ММ-ДД')]
    #[Assert\DateTime('Y-m-d', message: 'Невалидный формат даты, верный ГГГГ-ММ-ДД', groups: ['create'])]
    #[Groups(['create'])]
    public $dateBorn;

    #[Assert\Type(type: 'string', message: 'Неверный тип данных', groups: ['create'])]
    #[Assert\NotBlank(message: 'Значение не может быть пустым', groups: ['create'])]
    #[Property(type: 'string')]
    #[Groups(['create'])]
    public $login;

    #[Assert\Type(type: 'string', message: 'Неверный тип данных', groups: ['create'])]
    #[Assert\NotBlank(message: 'Значение не может быть пустым', groups: ['create'])]
    #[Property(type: 'string')]
    #[Groups(['create'])]
    public $password;
}