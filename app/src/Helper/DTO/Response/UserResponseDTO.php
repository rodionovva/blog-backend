<?php

namespace App\Helper\DTO\Response;

use App\Entity\User;
use Symfony\Component\Serializer\Annotation\Groups;

final readonly class UserResponseDTO
{
    #[Groups(['getUser'])]
    public ?int $id;

    #[Groups(['getUser'])]
    public ?string $name;

    #[Groups(['getUser'])]
    public ?string $dateBorn;

    #[Groups(['getUser'])]
    public ?string $login;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->id = $user->getId();
        $this->name = $user->getName();
        $this->dateBorn = $user->getDateBorn()->format('Y-m-d');
        $this->login = $user->getLogin();
    }
}