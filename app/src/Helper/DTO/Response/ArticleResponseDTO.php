<?php

namespace App\Helper\DTO\Response;

use App\Entity\Article;
use App\Entity\File;
use Symfony\Component\Serializer\Annotation\Groups;

final readonly class ArticleResponseDTO
{
    #[Groups(['getArticle'])]
    public ?int $id;

    #[Groups(['getArticle'])]
    public ?string $title;

    #[Groups(['getArticle'])]
    public ?string $content;

    #[Groups(['getFile'])]
    /** @var File[] $images */
    public ?array $images;

    /**
     * @param Article $article
     */
    public function __construct(Article $article)
    {
        $this->id = $article->getId();
        $this->title = $article->getTitle();
        $this->content = $article->getContent();
        $this->images = $article->getImages()->toArray();
    }
}