<?php

namespace App\Tests\Api;

use App\Entity\Device;
use App\Entity\User;
use App\Helper\Status\DeviceStatus;
use App\Tests\Support\ApiTester;
use Doctrine\ORM\EntityManagerInterface;

class AbstractApiCest
{
    protected EntityManagerInterface $entityManager;
    protected const BASE_PATH = '/api/v1';

    public function _before(ApiTester $I)
    {
        $this->entityManager = $I->grabService(EntityManagerInterface::class);
    }

    protected function findTestUser(): User
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'login' => 'vlados',
        ]);
        if (!$user) {
            $user = (new User())
                ->setLogin('vlados')
                ->setDateBorn(new \DateTime('1998-02-17'))
                ->setName('Vladislav')
                ->setSurname('Rodionov')
                ->setPassword('password');

            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
        return $user;
    }

    protected function findTestDevice(User $user): Device
    {
        foreach ($user->getDevices() as $device) {
            if ($device->getStatus() === DeviceStatus::ACTIVE->value) {
                return $device;
            }
        }

        $device = (new Device())
            ->setOwner($user);

        $this->entityManager->persist($device);
        $this->entityManager->flush();

        return $device;
    }

    protected function setToken(ApiTester $I): void
    {
        $device = $this->findTestDevice($this->findTestUser());
        $I->haveHttpHeader('apiKey', $device->getToken());
    }
}