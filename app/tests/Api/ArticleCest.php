<?php


namespace App\Tests\Api;

use App\Entity\Article;
use App\Tests\Support\ApiTester;
use Symfony\Component\HttpFoundation\Response;

class ArticleCest extends AbstractApiCest
{
    private const PATH = '/article';

    public function _before(ApiTester $I)
    {
        parent::_before($I);
    }

    private function getRandomArticle(): Article
    {
        $article = $this->entityManager->getRepository(Article::class)->findOneBy([]);

        if (!$article) {
            $article = (new Article())
                ->setContent(md5(microtime() . random_int(1, 1000)))
                ->setTitle(md5(microtime() . random_int(1, 1000)))
                ->setCreatedUser($this->findTestUser());
            $this->entityManager->persist($article);
            $this->entityManager->flush();
        }

        return $article;
    }

    // создание статьи
    public function testCreateArticle(ApiTester $I): void
    {
        $this->setToken($I);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost(self::BASE_PATH . self::PATH, [
            'title' => md5(microtime() . random_int(1, 1000)),
            'content' => md5(microtime() . random_int(1, 1000)),
        ]);
        $I->seeResponseCodeIs(Response::HTTP_CREATED);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'title' => 'string',
            'content' => 'string',
            'images' => 'array',
        ]);
    }

    // получение всех статей
    public function testGetAllArticles(ApiTester $I): void
    {
        $this->setToken($I);
        $this->getRandomArticle();
        $I->sendGet(self::BASE_PATH . self::PATH);
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'title' => 'string',
            'content' => 'string',
            'images' => 'array',
        ]);
    }

    // получение статьи по id
    public function testGetArticleById(ApiTester $I): void
    {
        $this->setToken($I);
        $articleId = $this->getRandomArticle()->getId();
        $I->sendGet(self::BASE_PATH . self::PATH . "/$articleId");
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'title' => 'string',
            'content' => 'string',
            'images' => 'array',
        ]);
    }

    // получение статей текущего пользователя
    public function testGetArticlesCurrentUser(ApiTester $I): void
    {
        $this->setToken($I);
        $I->sendGet(self::BASE_PATH . self::PATH . '/user');
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'title' => 'string',
            'content' => 'string',
            'images' => 'array',
        ]);
    }

    // получение статей конкретного пользоввателя
    public function testGetArticlesUser(ApiTester $I): void
    {
        $this->setToken($I);
        $article = $this->getRandomArticle();
        $I->sendGet(self::BASE_PATH . self::PATH . '/user/' . $article->getCreatedUser()->getId());
        $I->seeResponseCodeIs(Response::HTTP_OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'title' => 'string',
            'content' => 'string',
            'images' => 'array',
        ]);
    }

    //Архивирование статьи
    public function testArchiveArticle(ApiTester $I): void
    {
        $this->setToken($I);
        $article = $this->getRandomArticle();
        $I->sendDelete(self::BASE_PATH . self::PATH . '/' . $article->getId());
        $I->seeResponseCodeIs(Response::HTTP_NO_CONTENT);
    }
}
