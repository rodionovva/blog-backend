1. Склонировать репозиторий на локальную машину
2. Зайти в папку проекта, собрать контейнеры: docker-compose up -d --build
3. Зайти в контейнер php: docker-compose exec php bash
4. Выполнить composer install
5. Накатить миграции: php bin/console doctrine:migration:migrate
6. Создать тестовую БД php bin/console doctrine:database:create --env=test
7. Накатить миграции тестовой БД php bin/console doctrine:migration:migrate --env=test
8. api doc доступен по адресу localhost:7070/api/v1/doc
9. Запустить тесты для апи vendor/bin/codecept run Api